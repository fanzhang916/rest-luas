const fastify = require('fastify')({ logger: false });
const Luas = require('luas');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;

// if (cluster.isMaster) {
//   console.log(`Master ${process.pid} is running`);
//   for (let i = 0; i < numCPUs; i++) {
//     cluster.fork();
//   }
//   cluster.on('exit', worker => {
//     console.log(`Worker ${worker.process.pid} died`);
//   });
// } else {
  // const policyFile = request.query.policy
  // const policyFiles = [`./test-data/policies/${policyFile}.xml`]

  (async () => {
    await Luas.loadPolicy([`./test-data/policies/continue.xml`]);
  })()

  fastify.get('/', async (request, reply) => {
    return "Rest Luas service is running"
  })

  fastify.get('/eval', async (request, reply) => {
    let { type, info } = request.query
    info = info.split(',')
    let req = './test-data/requests'
    if (type === 'continue') {
      const reqNO = Math.floor(Math.random() * 200) + 1
      req += `/${type}/${info[0]}/${reqNO}-req.xml`
    } else {
      req += `/xlarge/FirstApplicable-DistributedDeny/0.1/${info[0]}/test/${info[1]}/1/xml/${reqNO}.xml`
    }

    const decision = await Luas.getDecision(req)
    return decision
  })


  fastify.listen(3000, '0.0.0.0', function (err, address) {
    if (err) {
      fastify.log.error(err)
      process.exit(1)
    }
    fastify.log.info(`server listening on ${address}`)
  })
// }