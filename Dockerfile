FROM node:10-alpine

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install pm2 -g
RUN npm install
EXPOSE 3000
COPY . .

CMD [ "npm", "run", "start" ]